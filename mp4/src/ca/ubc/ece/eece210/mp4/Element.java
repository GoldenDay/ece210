package ca.ubc.ece.eece210.mp4;

import java.util.List;
import java.util.*;

/**
 * An abstract class to represent an entity in the catalogue. The element (in this
 * implementation) can either be an album or a genre.
 * 
 * @author 
 * 
 */
class LeafException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public LeafException() {
		super();
	}
}

public abstract class Element {
	private final List<Element> children = new ArrayList<Element>();
	
	/**
	 * returns the element's name
	 * @return the name of the element
	 */
	public abstract String getName();
	
	/**
	 * adds the element to a parent genre.
	 */
	
	public abstract void addToGenre( Genre g );
	
	/**
	 * returns a string representation of the element.
	 * @return a string rep of the element
	 */
	
	public abstract String getStringRepresentation();
	
	/**
	 * Abstract method to determine if a given entity can (or cannot) contain
	 * any children.
	 * 
	 * @return true if the entity can contain children, or false otherwise.
	 */
	public abstract boolean hasChildren();

	
	/**
	 * Abstract method to give type (album or genre) of current element
	 * 
	 * @return true if the entity is a genre, false otherwise
	 */
	public boolean isGenre() {
		if (this.hasChildren()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Abstract method to give type (album or genre) of current element
	 * 
	 * @return true if the entity is an album, false otherwise
	 */
	public boolean isAlbum() {
		if (!this.hasChildren()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns all the children of this entity in a list. They can be albums or
	 * genres. In this particular application, only genres can have
	 * children. Therefore, this method will return the albums or genres
	 * contained in this genre.
	 * 
	 * @requires this element is a genre at runtime
	 * 
	 * @return the children; null if no children
	 */
	public List<Element> getChildren() {
		//return the whole list
		return this.children;
	}

	/**
	 * Adds a child to this entity. Basically, it is adding an album or genre
	 * to an existing genre
	 * 
	 * @param b
	 *            the entity to be added.
	 * @modifies this.children
	 * @throws LeafException if the element is an album
	 */
	protected void addChild(Element b) throws LeafException {
		if (this.isAlbum()) {
			throw new LeafException();
		}
		this.children.add(b);
		// (should throw an exception if trying to add
		// to a leaf object);
	}
	
	/** 
	 * Removes a child (album) from the entity.  Basically, an album is removed from a genre.
	 * Caller is the genre from which album will be removed.
	 *
	 * @param b the entity to be removed
	 * @requires b assumed in this.children; b is a genre
	 * @modifies this.children
	 * @effects Element b is removed from this.children
	 * 
	 */
	protected void removeChild(Element b) {
		if (this.isGenre()) {
			this.children.remove(b); //List interface automatically checks for containment
			return;
		}
		return; //Calling object is an album
	}

	}