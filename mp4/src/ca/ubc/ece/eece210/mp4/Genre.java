package ca.ubc.ece.eece210.mp4;

/**
 * Represents a genre (or collection of albums/genres).
 * 
 * 
 */
public final class Genre extends Element {
    private String name = new String();
    private Genre parent;
        
	/**
	 * Creates a new genre with the given name.
	 * 
	 * @param name
	 *            the name of the genre.
	 */
	public Genre(String name) {
		this.name = name;
	}
	
	/**
	 * returns the name of the genre
	 * @return the name of the genre
	 */
	
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the parent genre of this genre
	 * @returns the parent genre
	 */
	
	public Genre getParent() {
		return this.parent;
	}
	
	/**
	 * Adds this genre to a parent genre
	 * 
	 * Note: For consistency issues, the method has been modified so that the caller itself is the genre being added. 
	 * 
	 * @param g parent genre to add this genre to
	 * @modifies this, this.parent (if it exists), g
	 * 
	 */
	
	public void addToGenre (Genre g) {
		//We need to check that this is not a FIRST add to genre
		if (this.parent != null) {
			this.parent.removeChild(this);
		}
		this.parent = g;
		try {
			g.addChild(this);
		} catch (LeafException e) {
			//this ain't gonna happen either
		}
	}

	/**
	 * Restores ONE genre from a given string representation.
	 * 
	 * @param stringRepresentation;
	 * @requires stringRepresentation is in form "<Genre>\tparentName\tgenreWithParent<\\Genre>" or "<Genre>\tgenreWithoutParent\t<\\Genre>"
	 * @return new genre object with the name given;
	 * 			null if stringRepresentation has invalid form
	 */
	public static Genre restoreGenre(String stringRepresentation) {
		
		String name = new String();
		String endOfString = new String();
		Boolean hasParent = true;
		
		/* Check that stringRepresentation is in valid form */
		if (stringRepresentation == null) {
			return null;
		}
		if (!stringRepresentation.contains("<Genre>") || !stringRepresentation.contains("<\\Genre>") || !stringRepresentation.contains("\t")) {
			return null;
		}
		
		// Break string at first tab and save end part
		int index1 = stringRepresentation.indexOf("\t");
		endOfString = stringRepresentation.substring(index1 + 1);
		
		/* Two possibilities for endOfString:
		 * Example 1: "parentName\tgenreWithParent<\\Genre>"
		 * Example 2: "genreWithoutParent\t<\\Genre>"
		 * 
		 * If \t and <\\Genre> adjacent, then no parent. Otherwise, there is a parent.
		 */
		int index2 = endOfString.indexOf("\t");
		int index3 = endOfString.indexOf("<\\Genre>");
		if (index3 == index2 + 1) {
			hasParent = false;
		}
		
		//Parse name - location dependent on existence of parent
		if (hasParent) {
			name = endOfString.substring(index2 + 1, index3);
		}
		else {
			name = endOfString.substring(0,index2);
		}
		
		// Construct and return new genre object with parsed name
		Genre restoredGenre = new Genre(name);
		return restoredGenre;
		
	}

	/**
	 * Returns the string representation of a genre
	 * 
	 * @return stringRepresentation of genre in form "<Genre>\tparentName\tgenreWithParent<\\Genre>" or "<Genre>\tgenreWithoutParent\t<\\Genre>"
	 */
	public String getStringRepresentation() {
		String stringRep;
		if (parent == null){
			stringRep = "<Genre>" + "\t" + name + "\t" + "<\\Genre>";
		}else{
			stringRep = "<Genre>" + "\t" + parent.getName() + "\t" + name + "<\\Genre>"; 
		}
		return stringRep;
	}

	/**
	 * Returns true, since a genre can contain other albums and/or
	 * genres.
	 */
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	/**
	 * Indicates whether or not another object is equal to this Genre.
	 * In this case, returns true if the object passed is a genre with the same name and artist as the genre this is called on.
	 * @return true if the objects are equal, false otherwise
	 */
	
	@Override
	public boolean equals(Object o) {
		if (o.getClass() != this.getClass()){
			return false;
		}
		
		Genre g = (Genre)o;
		if ((g.getName() != this.getName())||(g.getParent() != this.getParent())){
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		if (this.getParent() == null) {
			return this.getName().hashCode();
		}
		return this.getName().hashCode() + this.getParent().hashCode();
	}
}

