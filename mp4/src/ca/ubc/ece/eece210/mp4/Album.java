package ca.ubc.ece.eece210.mp4;

import java.util.ArrayList;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 * This class contains the information needed to represent 
 * an album in our application.
 * 
 */

public final class Album extends Element {
	private String title = new String();
	private String artist = new String();
	private ArrayList<String> songList = new ArrayList<String>();
	private Genre genre = null;
	/**
	 * Builds an album with the given title, performer and song list
	 * 
	 * @param title
	 *            the title of the album
	 * @param author
	 *            the performer 
	 * @param songlist
	 * 			  the list of songs in the album
	 */
	public Album(String title, String performer, ArrayList<String> songlist) {
		this.title = title;
		this.artist = performer;
		this.songList = songlist;
		
	}

	/**
	 * Builds an album from the string representation of the object. It is used
	 * when restoring an album from a file. NOTE: This does not restore the genre field. That must be done externally.
	 * 
	 * @requires //Put BASIC FORM OF STRING REP here
	 * @param stringRepresentation
	 *            the string representation of the album, must include the ending </Album> tag.
	 */
	public Album(String stringRep) {
		String workString = stringRep.substring(stringRep.indexOf("\t") + 1);;
		this.title = workString.substring(0, workString.indexOf("\t"));
		workString = workString.substring(workString.indexOf("\t") + 1);
		this.artist = workString.substring(0, workString.indexOf("\t"));
		workString = workString.substring(workString.indexOf("\t") + 1);
		while (workString.indexOf("<\\Album>") != 0){
			songList.add(workString.substring(0, workString.indexOf("\t")));
			workString = workString.substring(workString.indexOf("\t") + 1);
		}
	}

	/**
	 * Returns the string representation of the given album in the form "Artist - Album: song1, song2, ... , songN".
	 * 
	 * @return the string representation
	 */
	public String getStringRepresentation() {
		String stringRep = "<Album>" + genre.getName() + "\t" + title + "\t" + artist + "\t";
		for (String song : songList) {
			stringRep = stringRep.concat(song + "\t");
		}
		stringRep = stringRep.concat("<\\Album>");
		return stringRep;
	}

	/**
	 * Add the album to the given genre
	 * Album is the calling object.
	 * 
	 * @modifies genre, this
	 * @param genre
	 *            the genre to add the album to.
	 */
	public void addToGenre(Genre genre) {
		this.removeFromGenre();
		this.genre = genre;
		try {
			genre.addChild(this);
		} catch (LeafException e) {
			//this won't happen either
		}
	}
	
	/**
	 * Removes the album from its current genre, if there is one.
	 * Calling object is the album.
	 * 
	 * @modifies genre
	 */
	
	public void removeFromGenre(){
		if (genre != null) {
			genre.removeChild(this);
			genre = null;
		}
	}

	/**
	 * Returns the genre that this album belongs to.
	 * 
	 * @return the genre that this album belongs to; null if this has no genre
	 */
	public Genre getGenre() {
		return genre;
	}
	
	/**
	 * A method to get the song list of the album
	 * @return an ArrayList of strings with all the songs on the album
	 */
	
	public ArrayList<String> getSongList() {
		return songList;
	}

	/**
	 * Returns the title of the album
	 * 
	 * @return the title
	 */
	public String getName() {
		return title;
	}

	/**
	 * Returns the performer of the album
	 * 
	 * @return the performer
	 */
	public String getPerformer() {
		return artist;
	}

	/**
	 * An album cannot have any children (it cannot contain anything).
	 */
	@Override
	public boolean hasChildren() {
		return false;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o.getClass() != this.getClass()){
			return false;
		}
		
		Album a = (Album)o;
		if ((a.getName() != this.getName())||(a.getPerformer() != this.getPerformer())){
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		return title.hashCode() + artist.hashCode();
	}
}
