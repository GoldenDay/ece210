package ca.ubc.ece.eece210.mp4.ast;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.Element;
import ca.ubc.ece.eece210.mp4.Catalogue;

public class MatchesNode extends ASTNode {

    public MatchesNode(Token token) throws InvalidTokenException {
    	if (token.getType() != TokenType.MATCHES){
    		throw new InvalidTokenException();
    	}
    	super.token = token;
    }
    
    public MatchesNode(Token token, String arguments) throws InvalidTokenException {
    	if (token.getType() != TokenType.MATCHES){
    		throw new InvalidTokenException();
    	}
    	super.token = token;
    	super.setArguments(arguments);
    }

    /**
     * This method interprets the MatchesNode and returns the set of Elements 
     * that satisfies the conditions of the MatchesNode.
     * 
     * @return Set of Elements that satisfy the criteria indicated by the MatchesNode.
     */
    
    @Override
    public Set<Element> interpret(Catalogue argument) {
    	return getElements(argument.getRoot());
    }
    
    /**
     * Takes an element and checks if it or its children satisfy the argument & node type
     * @param root root element for all elements to be checked
     * @return set of all elements that satisfy the argument and node type
     */
    
    public Set<Element> getElements(Element root){
    	Set<Element> result = new HashSet<Element>();
    	if (root.isAlbum() && root.getName().toLowerCase().contains(arguments.toLowerCase())){
    		result.add(root);
    	}
    	if (root.isGenre()){
	    	List<Element> children = root.getChildren();
	    	for (Element child : children){
	    		result.addAll(getElements(child));
	    	}
    	}
		return result;
    }
    
    /**
     * This method does nothing for the MatchesNode class.
     */
    
    @Override
    public void addChild (ASTNode node){
    	//do nothing, this node cannot have children
    }

}
