package ca.ubc.ece.eece210.mp4.ast;

public enum TokenType {
    ROOT, AND, BY, OR, STRING, MATCHES, IN, END, L_PARAN, R_PARAN;
}
