package ca.ubc.ece.eece210.mp4.ast;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.Album;
import ca.ubc.ece.eece210.mp4.Catalogue;
import ca.ubc.ece.eece210.mp4.Element;
import ca.ubc.ece.eece210.mp4.Genre;

public class ByNode extends ASTNode {
	
	/**
	 * Constructs a ByNode given correct "by" token.
	 * 
	 * @param token
	 * @throws InvalidTokenException
	 */
	public ByNode(Token token) throws InvalidTokenException {
		if (token.getType() != TokenType.BY){
			throw new InvalidTokenException();
		}
		super.token = token;
		
	}
	
	/**
	 * addChild() does nothing for ByNode class, leaf nodes have no children
	 */
	@Override
	public void addChild(ASTNode node){
		//do nothing
		return;
	}
	
	/**
	 * Searches through provided catalogue and returns set containing all elements meeting search
	 * criteria of this node: Albums having author specified by "this.arguments"
	 * 
	 * Example: this.argument = "Daniel Sedin" : returns a set of all albums by Daniel Sedin
	 * 
	 * @param argument (Catalogue to be searched)
	 * @return set containing albums by this.arguments
	 */
	@Override
	public Set<Element> interpret(Catalogue argument) {
		
		Set<Element> bySearchResult = new HashSet<Element>();
		Genre root = argument.getRoot();
		findAlbumsFromRoot(root, bySearchResult);
		
		return bySearchResult;
	}
	
	/**
	 * A private helper method searches tree for specified albums, given root element.
	 * 
	 * @param root: root of the tree
	 * @param searchResults: set to store matching albums
	 */
	private void findAlbumsFromRoot(Element root, Set<Element> searchResults) {
		
		if(root.isAlbum()) {
			Album temp = (Album)root;
			if(temp.getPerformer().equals(arguments)){
				searchResults.add(temp);
			}
		}
		
		if (root.isGenre()){
			List<Element> tempChildrenList;
			if(root.getChildren() != null) { //probably could do this check in a more meaningful way
				tempChildrenList = root.getChildren();
				for (Element children : tempChildrenList) {
					findAlbumsFromRoot(children, searchResults);
				}
			}
		}	
		//Note returning from other paths is implicit. -implicitly returns from all paths
	}

}
