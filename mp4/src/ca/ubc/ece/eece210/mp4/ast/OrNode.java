package ca.ubc.ece.eece210.mp4.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.Element;
import ca.ubc.ece.eece210.mp4.Catalogue;

/**
 * 
 * @author Vincent Hui
 *
 */

public class OrNode extends ASTNode {

	/**
	 * Constructs an OrNode given correct "or" token.
	 * 
	 * @param token
	 * @throws InvalidTokenException
	 */
	public OrNode(Token token) throws InvalidTokenException {
		if (token.getType() != TokenType.OR){
			throw new InvalidTokenException();
		}
		
		super.token = token;
		
	}
	
	/**
     * This method sets the arguments for the node.
     * Does Nothing for the OrNode - non leaf node
     * 
     * @requires this node is a leaf node (ie. by, matches, in) at runtime
     * @param arguments: a String that represents the arguments
     */
    public void setArguments(String arguments) {
    	return;
    }

    /**
     * This method interprets the ORNode and returns the set of Elements 
     * that satisfies the conditions of the ORNode.
     * 
     * @requires this has two childNodes
     * @return Set of Elements that satisfy the criteria indicated by the ORNode.
     */
    @Override
    public Set<Element> interpret(Catalogue argument) {
    	
    	Set<Element> tempSet = new HashSet<Element>();
    	for (ASTNode child : children){
			tempSet.addAll(child.interpret(argument));
		}
    	return tempSet;
    	
    	
    }

}