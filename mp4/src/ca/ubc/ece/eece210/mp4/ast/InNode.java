package ca.ubc.ece.eece210.mp4.ast;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.*;

public class InNode extends ASTNode {

    public InNode(Token token) throws InvalidTokenException {
    	if (token.getType() != TokenType.IN){
    		throw new InvalidTokenException();
    	}
    	super.token = token;
    }
    
    public InNode(Token token, String arguments) throws InvalidTokenException {
    	if (token.getType() != TokenType.IN){
    		throw new InvalidTokenException();
    	}
    	super.token = token;
    	super.setArguments(arguments);
    }

    /**
     * This method interprets the InNode and returns the set of Elements 
     * that satisfies the conditions of the InNode.
     * 
     * @return Set of Elements that satisfy the criteria indicated by the InNode.
     */
    
    @Override
    public Set<Element> interpret(Catalogue argument) {
    	return getElements(argument.getRoot());
    }
    
    /**
     * Searches for the genre specified in the payload and returns any albums within it
     * @param root root of all the elements that need to be checked
     * @return a set of all the elements under the root that are in the payload genre
     */
    
    private Set<Element> getElements(Element root){
    	Set<Element> result = new HashSet<Element>();
    	
		if (root.isGenre()){
			if (root.getName().equalsIgnoreCase(arguments)){//this is the genre we're looking for
				result.addAll(getAlbums(root));//return all albums within this genre
			} else {//this isn't the genre we're looking for
				List<Element> children = root.getChildren();
				for (Element child : children){//check children to find the genre
					result.addAll(getElements(child));
				}
			}
		}
		return result;
    }
    
    /**
     * returns all albums that are directly below the given root in the tree
     * @param root the root to return all the children of
     * @return a set of all the albums that are directly below the root
     */
    
    private Set<Element> getAlbums (Element root){
    	Set<Element> temp = new HashSet<Element>();
    	if (root.isGenre()){
    		List<Element> children = root.getChildren();
    		for (Element child : children){
    			if (child.isAlbum()){
    				temp.add(child);
    			}
    			if (child.isGenre()){
    				temp.addAll(getAlbums(child));
    			}
    		}
    	} else {
    		temp.add(root);
    	}
    	return temp;
    }
    
    /**
     * This method does nothing for the MatchesNode class.
     */
    
    @Override
    public void addChild (ASTNode node){
    	//do nothing, this node cannot have children
    }

}
