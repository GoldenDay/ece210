package ca.ubc.ece.eece210.mp4.ast;


import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.Element;
import ca.ubc.ece.eece210.mp4.Catalogue;

/**
 * 
 * @author Vincent Hui
 *
 */

public class AndNode extends ASTNode {

	/**
	 * Constructs an AndNode given correct "and" token.
	 * 
	 * @param token
	 * @throws InvalidTokenException
	 */
	public AndNode(Token token) throws InvalidTokenException {
		if (token.getType() != TokenType.AND){
			throw new InvalidTokenException();
		}
		//super.children = new ArrayList<ASTNode>(2);
		super.token = token;
		
	}
	
	/**
     * This method sets the arguments for the node.
     * Does Nothing for the AndNode - non leaf node
     * 
     * @requires this node is a leaf node (ie. by, matches, in) at runtime
     * @param arguments: a String that represents the arguments
     */
    public void setArguments(String arguments) {
    	return;
    }

    /**
     * This method interprets the ANDNode and returns the set of Elements 
     * that satisfies the conditions of the ANDNode.
     * 
     * @requires this has two childNodes
     * @return Set of Elements that satisfy the criteria indicated by the ANDNode.
     */
    @Override
    public Set<Element> interpret(Catalogue argument) {
    	
    	Set<Element> tempSet = new HashSet<Element>();
    	tempSet = super.children.get(0).interpret(argument); //get first childNode's elements
    	tempSet.retainAll(super.children.get(1).interpret(argument) ); //intersect with second childNode's elements
    	return tempSet;
    	
    	
    }

}
