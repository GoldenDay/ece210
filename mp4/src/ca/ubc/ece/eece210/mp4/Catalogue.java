package ca.ubc.ece.eece210.mp4;

import java.io.*;
import java.util.*;
import ca.ubc.ece.eece210.mp4.ast.ASTNode;
import ca.ubc.ece.eece210.mp4.ast.QueryParser;
import ca.ubc.ece.eece210.mp4.ast.QueryTokenizer;
import ca.ubc.ece.eece210.mp4.ast.Token;

/**
 * Container class for all the albums and genres. Its main 
 * responsibility is to save and restore the collection from a file.
 * When using this implementation of collection, it is important to assign all top genres to be children of the root - "Music"
 * 
 * Top-level genres and unclassified albums must all be stored in the two Lists: albums and genres
 * 
 * @author Dylan Kirkby
 * 
 */
public final class Catalogue {
	private ArrayList<Element> elements;
	private Genre root;

	/**
	 * Returns the root of the catalogue.
	 * @return the root of the catalogue
	 */
	
	public Genre getRoot() {
		return root;
	}

	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		elements = new ArrayList<Element>();
		root = new Genre("root");
	}
	
	/**
	 * Builds a new catalogue and restores its contents from the 
	 * given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 */
	public Catalogue(String fileName) {
		try {
			recreateLibraryFromFile(fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * A private method to implement traversal by recursion.
	 * 
	 * @param Element e the element to search through for printing names; File file, the file to print names to
	 * 
	 * @requires No modification to genre and tree between the calls
	 * @effects Prints out every traversed element
	 * 
	 */
	public void saveCatalogueFromRoot(Element e, PrintStream output) {
		List<Element> childrenToSearch = new ArrayList<Element>();
		if (e.isGenre()){
			output.println(e.getStringRepresentation());
			childrenToSearch = e.getChildren();
			//iterate through local array and recursively check through its elements
			for(Element children: childrenToSearch) {
				saveCatalogueFromRoot(children, output);
			}
		}
			
		if (e.isAlbum()){
			output.println(e.getStringRepresentation());
		}
	}
	
	/**
	 * Saves the entire catalogue to a file
	 * @param output PrintStream to the output file
	 */
	
	public void saveCatalogueFromRoot (PrintStream output) {
		saveCatalogueFromRoot (root, output);
	}
	
	/**
	 * Adds an element and all its children to the catalogue
	 * @param e elements to add
	 */
	
	public void addToCatalogue (Element e) {
		e.addToGenre(root);
	}
	
	/**
	 * Saves the catalogue, from a root element down, to a file
	 * @param e root element
	 * @param fileName the location of the file
	 */
	
	public void saveCatalogueToFile(Element e, String fileName) {
		PrintStream output;
		try {
			output = new PrintStream(fileName);
			saveCatalogueFromRoot(e, output);
		} catch (FileNotFoundException e1) {
			System.out.println("File not found.");
		}
		
	}
	
	/**
	 * @ param query
	 * @ return ArrayList of albums that match query
	 */
	public Set<Element> queryCat (String query) {
		Set<Element> myQueryAlbumSet = new HashSet<Element>();
		
		List<Token> tokens = QueryTokenizer.tokenizeInput(query);
		QueryParser parser = new QueryParser(tokens);
		
		ASTNode root = parser.getRoot();
		myQueryAlbumSet = root.interpret(this);
		
		return myQueryAlbumSet; 
	}
	
	
	/**
	 * saves the entire catalogue to a specified file
	 * @param fileName name of the file to save to
	 */
	
	public void saveCatalogueToFile(String fileName){
		saveCatalogueToFile(elements.get(0), fileName);
	}
	
	/**
	 * Recreates the contents of a catalogue from a given file
	 * @requires Top-level nodes must be listed before elements in left and right subtree
	 * @param fileName path to the file location
	 * @modifies input
	 * @throws FileNotFoundException if the specified file doesn't exist
	 */
	public void recreateLibraryFromFile(String fileName) throws FileNotFoundException {
		Scanner input = new Scanner(new File(fileName));

		while (input.hasNextLine()){
			String line = input.nextLine();
			if (line.contains("<Album>")){
				elements.add(new Album (line));//creates a new album from the string representation
				elements.get(elements.size() - 1).addToGenre(root);
				for (Element g : elements){
					if (g.isGenre()){
						g = ((Genre)g);
						if (g.getName().equals(line.substring(7, 7 + g.getName().length()))){
							elements.get(elements.size() - 1).addToGenre((Genre)g);
							break;
						}
					}
				}
			}
			if (line.contains("<Genre>")){
				elements.add(Genre.restoreGenre(line));//creates a new genre from the string representation
				elements.get(elements.size() - 1).addToGenre(root);
				if (elements.size() > 1){
					for (Element g : elements){
						if (g.isGenre()){
							if (g.getName().equals(line.substring(8, 8 + g.getName().length()))){
								elements.get(elements.size() - 1).addToGenre((Genre)g);
								break;
							}
						}
					}
				}
			}
		}
		input.close();
	}
}
