package ca.ubc.ece.eece210.mp4.ast;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp4.Album;
import ca.ubc.ece.eece210.mp4.Catalogue;
import ca.ubc.ece.eece210.mp4.Element;
import ca.ubc.ece.eece210.mp4.Genre;

public class OrNodeTest {
	
	Catalogue testCatalogue = new Catalogue();
	
	Album Beethoven_Symphonies;
	Album Chopin_Nocturnes;
	Album Chopin_Symphonies;
	Album Jazz_Symphonies;
	Album Louis_and_the_Angels;
	Album Crossings;
	
	Genre Baroque;
	Genre Classical;
	Genre Romantic;
	Genre Jazz;
	Genre Psychedelic_Jazz;
	
	ArrayList<String> emptySongs = new ArrayList<String>();
	
	//instantiate some different andNodes, inNodes, matchesNode for checking
	ASTNode orTestRootNode; 
	ASTNode orTestSecondNode;
	ASTNode inTestNode;
	ASTNode inTestNode2;
	ASTNode matchTestNode;
			
	Set<Element> expectedSet;
	Set<Element> resultSet;

	
	@Before
	public void initialize() {
		
		Classical = new Genre("Classical");
		Baroque = new Genre("Baroque");
		Baroque.addToGenre(Classical);
		Romantic = new Genre("Romantic");
		Romantic.addToGenre(Classical);
		
		//should we be REQUIRED to add to Music? - NOTE: These genres not linked to music
		Jazz = new Genre("Jazz");
		Psychedelic_Jazz = new Genre("Psychedelic Jazz");
		Psychedelic_Jazz.addToGenre(Jazz);
		
		/*Ideally, we REQUIRE an addToCatalogue() method so the interpret() method can do its work nicely
		 *  - now available.  */
		testCatalogue.addToCatalogue(Jazz);
		testCatalogue.addToCatalogue(Classical);
		
		Beethoven_Symphonies = new Album("Symphonies","Beethoven", emptySongs);
		Beethoven_Symphonies.addToGenre(Classical);

		Chopin_Nocturnes = new Album("Nocturnes", "Chopin", emptySongs);
		Chopin_Nocturnes.addToGenre(Romantic);
		
		Chopin_Symphonies = new Album("Chopin Symphonies", "Chopin", emptySongs); //assume that matches works with regexes correctly
		Chopin_Symphonies.addToGenre(Romantic);
		
		Jazz_Symphonies = new Album("Jazz Symphonies", "Whoever", emptySongs);
		Jazz_Symphonies.addToGenre(Jazz);
		
		Crossings = new Album("Crossings", "Herbie Hancock", emptySongs);
		Crossings.addToGenre(Psychedelic_Jazz);
		
		Louis_and_the_Angels = new Album("Louis and the Angels", "Louis Armstrong", emptySongs);
		Louis_and_the_Angels.addToGenre(Jazz);
		
	}
	
	//Thought: What if I used the wrong token to construct this node? - OK, won't test this, LOL
	
	@Test
	public void setArgumentTest() {
		try {
			orTestRootNode = new OrNode( Token.getTokenInstance("||") );
			orTestRootNode.setArguments("something");
			assertNull("OrNode should not have argument - rep invar is broken.", orTestRootNode.arguments);
		} catch (InvalidTokenException e) {
			//will not happen
		}
		
	}
	
	
	@Test
	public void addChildTest() {
		try {
			orTestRootNode = new OrNode( Token.getTokenInstance("||") );
			orTestSecondNode = new OrNode( Token.getTokenInstance("||") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			orTestRootNode.addChild(inTestNode);
			orTestRootNode.addChild(matchTestNode);
			orTestRootNode.addChild(orTestSecondNode);
			assertEquals("OrNode should only have 2 children - rep. invar broken", 2, orTestRootNode.children.size());
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	
	
	/**
	 * Note: The following tests instantiate inNode, and matchesNode objects, 
	 * and requires respective interpret() functioning properly for these tests to pass.
	 */
	@Test
	public void testInterpretSingleMatch() {
		try {
			expectedSet = new HashSet<Element>();
			expectedSet.add(Crossings);
			expectedSet.add(Jazz_Symphonies); //new
			expectedSet.add(Louis_and_the_Angels);
			
			//in Jazz or matches Crossings, Note: these NOT linked to music
			orTestRootNode = new OrNode( Token.getTokenInstance("||") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Jazz");
			matchTestNode.setArguments("Crossings");
			
			orTestRootNode.addChild(inTestNode);
			orTestRootNode.addChild(matchTestNode);
			resultSet = orTestRootNode.interpret(testCatalogue);
			
			//JUnit assertEquals(expected, actual) uses the Set.equals method
			assertEquals(expectedSet, resultSet);
		} catch (InvalidTokenException e) {
			//this won't happen
		}
		
	}
	
	@Test
	public void testInterpretOneEmptyNode() {
		try {
			expectedSet = new HashSet<Element>();
			expectedSet.add(Chopin_Nocturnes);
			
			//in Baroque or matches Nocturne - disjoint union
			orTestRootNode = new OrNode( Token.getTokenInstance("||") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Baroque");
			matchTestNode.setArguments("Nocturnes");
			
			orTestRootNode.addChild(inTestNode);
			orTestRootNode.addChild(matchTestNode);
			resultSet = orTestRootNode.interpret(testCatalogue);
			
			//JUnit assertEquals(expected, actual) uses the Set.equals method
			assertEquals(expectedSet, resultSet);
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	@Test
	public void testInterpretTwoEmptyNodes() {
		try {
			//in Baroque or matches Something -empty union with empty
			orTestRootNode = new OrNode( Token.getTokenInstance("||") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Baroque");
			matchTestNode.setArguments("Something");
				
			orTestRootNode.addChild(inTestNode);
			orTestRootNode.addChild(matchTestNode);
			resultSet = orTestRootNode.interpret(testCatalogue);
				
			assertTrue(resultSet.isEmpty());
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	@Test
	public void testInterpretMultipleMatch() {
		try {
			//also tests partial overlap
			expectedSet = new HashSet<Element>();
			expectedSet.add(Chopin_Nocturnes);
			expectedSet.add(Beethoven_Symphonies);
			expectedSet.add(Chopin_Symphonies);
			//expectedSet.add(Jazz_Symphonies);
			
			//in Classical or matches Symphonies
			orTestRootNode = new OrNode( Token.getTokenInstance("||") );
			inTestNode = new InNode (Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Classical");
			matchTestNode.setArguments("Nocturnes");
			
			orTestRootNode.addChild(inTestNode);
			orTestRootNode.addChild(matchTestNode);
			resultSet = orTestRootNode.interpret(testCatalogue);
			
			//JUnit assertEquals(expected, actual) uses the Set.equals method
			assertEquals(expectedSet, resultSet);
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	@Test
	public void testInterpretNestedOrCall() {
		try {
			//also tests full overlap
			expectedSet = new HashSet<Element>();
			expectedSet.add(Chopin_Symphonies);
			expectedSet.add(Beethoven_Symphonies);
			expectedSet.add(Chopin_Nocturnes);
			expectedSet.add(Jazz_Symphonies);
			
			//in Classical OR in Romantic OR matches Nocturnes
			orTestRootNode = new OrNode( Token.getTokenInstance("||") );
			orTestSecondNode = new OrNode( Token.getTokenInstance("||") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			inTestNode2 = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Classical");
			matchTestNode.setArguments("Symphonies");
			
			orTestSecondNode.addChild(inTestNode);
			orTestSecondNode.addChild(matchTestNode);
			
			inTestNode2.setArguments("Romantic");
			
			orTestRootNode.addChild(inTestNode2);
			orTestRootNode.addChild(orTestSecondNode);
			
			resultSet = orTestRootNode.interpret(testCatalogue);
			for (Element e : resultSet){
				System.out.println(e.getStringRepresentation());
			}
		
			//JUnit assertEquals(expected, actual) uses the Set.equals method
			assertEquals(expectedSet, resultSet);
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
}
