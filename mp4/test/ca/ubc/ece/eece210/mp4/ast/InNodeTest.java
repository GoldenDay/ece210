package ca.ubc.ece.eece210.mp4.ast;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.*;

import org.junit.Before;
import org.junit.Test;

public class InNodeTest {
        Catalogue yourMom = new Catalogue();
        Genre elec = new Genre("Electronic");
        Genre metal = new Genre("Metal");
        Genre rock = new Genre("Rock");
        Genre trance = new Genre("Trance");
        Genre edm = new Genre("EDM");
        Album aviciiTrue = new Album("True", "Avicii", null);
        Album loveElectric = new Album("Love Electric", "Circ", null);
        Album avengedSevenfold = new Album("Avenged Sevenfold", "Avenged Sevenfold", null);
        Album cityOfEvil = new Album("City of Evil Life", "Avenged Sevenfold", null);
        Album dayAndAge = new Album("Day & Age", "The Killers", null);
        Album elementsOfLife = new Album("Elements of Life", "Tiesto", null);
        Album whph = new Album("Work Hard Play Hard", "Tiesto Life", null);
        
        @Before
        public void before(){
                yourMom.addToCatalogue(rock);
                metal.addToGenre(rock);
                
                dayAndAge.addToGenre(rock);
                
                avengedSevenfold.addToGenre(metal);
                cityOfEvil.addToGenre(metal);
                
                yourMom.addToCatalogue(elec);
                trance.addToGenre(elec);
                edm.addToGenre(elec);
                loveElectric.addToGenre(elec);
                
                aviciiTrue.addToGenre(edm);
                whph.addToGenre(edm);
                
        }

        @Test
        public void interpretTest() {
        		String myQuery = ("Rock");
        		Set<Element> expectedResult = new HashSet<Element>();
        		InNode testNode;
    			try {
    				testNode = new InNode(Token.getTokenInstance("in"), myQuery);
    				Set<Element> searchResults = testNode.interpret(yourMom);
    				
    				expectedResult.add(cityOfEvil);
    				expectedResult.add(dayAndAge);
    				expectedResult.add(avengedSevenfold);
    				assertEquals(expectedResult, searchResults);
    			} catch (InvalidTokenException e){
    				e.printStackTrace();
    				fail();
    			}
        }

    	@Test
    	public void interpretTest0Results() {
    			String myQuery = ("Trance" );
    			InNode testNode;
    			try {
    				testNode = new InNode(Token.getTokenInstance("in"), myQuery);
    				Set<Element> searchResults = testNode.interpret(yourMom);
    				assertTrue (searchResults.isEmpty());
    			} catch (InvalidTokenException e){
    				e.printStackTrace();
    				fail();
    			}
    			
    	}
    	
    	@Test
    	public void nullTest(){
    			String myQuery = ("");
    			InNode testNode;
    			try {
    				testNode = new InNode(Token.getTokenInstance("IN"), myQuery);
    				Set<Element> searchResults = testNode.interpret(yourMom);
    				assertTrue (searchResults.isEmpty());
    			} catch (InvalidTokenException e){
    				e.printStackTrace();
    				fail();
    			}
    		}
    }
