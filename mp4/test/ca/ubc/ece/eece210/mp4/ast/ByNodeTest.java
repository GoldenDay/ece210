package ca.ubc.ece.eece210.mp4.ast;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.*;

import org.junit.Before;
import org.junit.Test;

public class ByNodeTest {
	Catalogue yourMom = new Catalogue();
	Genre elec = new Genre("Electronic");
	Genre metal = new Genre("Metal");
	Genre rock = new Genre("Rock");
	Genre trance = new Genre("Trance");
	Genre edm = new Genre("EDM");
	Album aviciiTrue = new Album("True", "Avicii", null);
	Album loveElectric = new Album("Love Electric", "Circ", null);
	Album avengedSevenfold = new Album("Avenged Sevenfold", "Avenged Sevenfold", null);
	Album cityOfEvil = new Album("City of Evil", "Avenged Sevenfold", null);
	Album dayAndAge = new Album("Day & Age", "The Killers", null);
	Album elementsOfLife = new Album("Elements of Life", "Tiesto", null);
	Album whph = new Album("Work Hard Play Hard", "Tiesto", null);
	
	
	
	@Before
	public void before(){
		yourMom.addToCatalogue(elec);
		yourMom.addToCatalogue(rock);
		metal.addToGenre(rock);
		trance.addToGenre(elec);
		edm.addToGenre(elec);
		aviciiTrue.addToGenre(edm);
		loveElectric.addToGenre(elec);
		avengedSevenfold.addToGenre(metal);
		cityOfEvil.addToGenre(metal);
		dayAndAge.addToGenre(rock);
		elementsOfLife.addToGenre(trance);
		whph.addToGenre(edm);
	}

	@Test
	public void interpretTest2Results() {
		try {
			ByNode testNode = new ByNode(Token.getTokenInstance("BY"));
			testNode.setArguments("Tiesto");
			Set<Element> testResult = testNode.interpret(yourMom);
			Set<Element> result = new HashSet<Element>();
			result.add(elementsOfLife);
			result.add(whph);
			assertEquals(result, testResult);
		} catch (InvalidTokenException e) {
			//won't happen
		}
	}
	
	@Test
	public void interpretTest1Result() {
		try {
			ByNode testNode = new ByNode(Token.getTokenInstance("BY"));
			testNode.setArguments("Avenged Sevenfold");
			Set<Element> testResult = testNode.interpret(yourMom);
			Set<Element> result = new HashSet<Element>();
			result.add(cityOfEvil);
			assertEquals(result, testResult);
		} catch (InvalidTokenException e) {
			//no
		}
	}
	
	@Test
	public void interpretTest0Results() {
		try {
			ByNode testNode = new ByNode(Token.getTokenInstance("BY"));
			testNode.setArguments("Baroness");
			Set<Element> testResult = testNode.interpret(yourMom);
			assertEquals(testResult, null);
		} catch (InvalidTokenException e) {
			// no
		}
	}
	
	@Test
	public void nullTest(){
		try {
			ByNode testNode = new ByNode(Token.getTokenInstance("BY"));
			Set<Element> testResult = testNode.interpret(yourMom);
			assertEquals(testResult, null);
		} catch (InvalidTokenException e) {
			// no
		}
	}

}
