package ca.ubc.ece.eece210.mp4.ast;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp4.*;

import org.junit.Before;
import org.junit.Test;

public class MatchesNodeTest {
        Catalogue yourMom = new Catalogue();
        Genre elec = new Genre("Electronic");
        Genre metal = new Genre("Metal");
        Genre rock = new Genre("Rock");
        Genre trance = new Genre("Trance");
        Genre edm = new Genre("EDM");
        Album aviciiTrue = new Album("True", "Avicii", null);
        Album loveElectric = new Album("Love Electric", "Circ", null);
        Album avengedSevenfold = new Album("Avenged Sevenfold", "Avenged Sevenfold", null);
        Album cityOfEvil = new Album("City of Evil Life", "Avenged Sevenfold", null);
        Album dayAndAge = new Album("Day & Age", "The Killers", null);
        Album elementsOfLife = new Album("Elements of love Life", "Tiesto", null);
        Album whph = new Album("Love Work Hard Play Hard", "Tiesto Life", null);
        
        
        
        @Before
        public void before(){
                yourMom.addToCatalogue(rock);
                metal.addToGenre(rock);
                
                dayAndAge.addToGenre(rock);
                
                avengedSevenfold.addToGenre(metal);
                cityOfEvil.addToGenre(metal);
                
                yourMom.addToCatalogue(elec);
                trance.addToGenre(elec);
                edm.addToGenre(elec);
                loveElectric.addToGenre(elec);
                
                aviciiTrue.addToGenre(edm);
                whph.addToGenre(edm);

                elementsOfLife.addToGenre(trance);
                
        }

        @Test
        public void interpretTest() {
        	String myQuery = ("Love" );
        	MatchesNode testNode;
        	Set<Element> expectedResult = new HashSet<Element>();
			try {
				testNode = new MatchesNode(Token.getTokenInstance("matches"), myQuery);
				Set<Element> searchResults = testNode.interpret(yourMom);
	        	
				expectedResult.add(elementsOfLife);
				expectedResult.add(loveElectric);
				expectedResult.add(whph);
	        	assertEquals(expectedResult, searchResults);
			} catch (InvalidTokenException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail();
			}
        	
        }

}
