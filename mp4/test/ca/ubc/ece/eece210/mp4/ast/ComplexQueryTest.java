package ca.ubc.ece.eece210.mp4.ast;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp4.*;

public class ComplexQueryTest {
	
	Catalogue uberCat = new Catalogue();
	
	Genre Rock = new Genre("Rock");
	Genre Metal = new Genre("Metal");
	Genre Alt = new Genre("Alternative");
	Genre Dance = new Genre("Dance");
	Genre Elec = new Genre("Electronic");
	Genre Pop = new Genre("Pop");
	Genre Jazz = new Genre("Jazz");
	Genre Funk = new Genre("Funk");
	
	String coldplay = "Coldplay";
	Album XnY = new Album("X & Y", coldplay, null);
	Album Parachutes = new Album("Parachutes", coldplay, null);
	Album MX = new Album("Mylo Xyloto", coldplay, null);
	String daftPunk = "Daft Punk";
	Album RAM = new Album("Random Access Memories", daftPunk, null);
	Album Discovery = new Album("Discovery", daftPunk, null);
	Album Homework = new Album("Homework", daftPunk, null);
	String glitchMob = "The Glitch Mob";
	Album DTS = new Album("Drink The Sea", glitchMob, null);
	Album WCMTWS = new Album("We Can Make The World Stop", glitchMob, null);
	String ironMaiden = "Iron Maiden";
	Album Number = new Album("Number Of The Beast", ironMaiden, null);
	Album POM = new Album("Piece Of Mind", ironMaiden, null);
	Album Powerslave = new Album("Powerslave", ironMaiden, null);
	String joeHend = "Joe Henderson";
	Album PageOne = new Album("Page One", joeHend, null);
	String justice = "Justice";
	Album Cross = new Album("Cross", justice, null);
	Album AVD = new Album("Audio, Video, Disco", justice, null);
	String katyPerry = "Katy Perry";
	Album OOTB = new Album("One Of The Boys", katyPerry, null);
	Album TeenDream = new Album("Teenage Dream", katyPerry, null);
	String ludique = "Ludique";
	Album Ropes = new Album("Learning The Ropes", ludique, null); 
	String masu = "Machinae Supremacy";
	Album Redeemer = new Album("Redeemer", masu, null);
	Album Overworld = new Album("Overworld", masu, null);
	Album Origin = new Album("Origin", masu, null);
	String matZo = "Mat Zo";
	Album DmgCtl = new Album("Damage Control", matZo, null);
	String metallica = "Metallica";
	Album RTL = new Album("Ride The Lightning", metallica, null);
	Album MetallicaAlb = new Album("Metallice", metallica, null);
	String metric = "Metric";
	Album Synthetica = new Album("Synthetica", metric, null);
	Album Fantasies = new Album("Fantasies", metric, null);
	String muse = "Muse";
	Album Absolution = new Album("Absolution", muse, null);
	Album Resistance = new Album("The Resistance", muse, null);
	Album secLaw = new Album("The 2nd Law", muse, null);
	String ptx = "Pentatonix";
	Album Vol1 = new Album("PTX Vol. 1", ptx, null);
	Album Vol2 = new Album("PTX Vol. 2", ptx, null);
	String rihanna = "Rihanna";
	Album Loud = new Album("Loud", rihanna, null);
	Album AGLM = new Album("A Girl Like Me", rihanna, null);
	String rush = "Rush";
	Album Presto = new Album("Presto", rush, null);
	Album Rush = new Album("Rush", rush, null);
	Album n2112 = new Album("2112", rush, null);
	Album Signals = new Album("Signals", rush, null);
	String steview = "Stevie Wonder";
	Album Innerv = new Album("Innervisions", steview, null);
	Album TalkingBook = new Album("Talking Book", steview, null); 
	String tiesto = "Tiesto";
	Album ElemsOfLife = new Album("Elements Of Life", tiesto, null);
	Album SrchOfSunrise = new Album("In Search Of Sunrise", tiesto, null);
	String theWho = "The Who";
	Album WhosNext = new Album("Who's Next", theWho, null);
	Album Tommy = new Album("Tommy", theWho, null);
	
	@Before
	public void before() {
		Metal.addToGenre(Rock);
		Funk.addToGenre(Jazz);
		Dance.addToGenre(Elec);
		XnY.addToGenre(Alt);
		MX.addToGenre(Alt);
		Parachutes.addToGenre(Alt);
		RAM.addToGenre(Dance);
		Discovery.addToGenre(Dance);
		Homework.addToGenre(Dance);
		DTS.addToGenre(Elec);
		WCMTWS.addToGenre(Elec);
		Number.addToGenre(Metal);
		POM.addToGenre(Metal);
		Powerslave.addToGenre(Metal);
		PageOne.addToGenre(Jazz);
		Cross.addToGenre(Elec);
		AVD.addToGenre(Elec);
		OOTB.addToGenre(Pop);
		TeenDream.addToGenre(Pop);
		Ropes.addToGenre(Elec);
		Redeemer.addToGenre(Metal);
		Overworld.addToGenre(Metal);
		Origin.addToGenre(Metal);
		DmgCtl.addToGenre(Dance);
		RTL.addToGenre(Metal);
		MetallicaAlb.addToGenre(Metal);
		Synthetica.addToGenre(Alt);
		Fantasies.addToGenre(Alt);
		Absolution.addToGenre(Alt);
		Resistance.addToGenre(Alt);
		Vol1.addToGenre(Pop);
		Vol2.addToGenre(Pop);
		Loud.addToGenre(Pop);
		AGLM.addToGenre(Pop);
		Presto.addToGenre(Rock);
		Rush.addToGenre(Rock);
		n2112.addToGenre(Rock);
		Signals.addToGenre(Rock);
		Innerv.addToGenre(Funk);
		TalkingBook.addToGenre(Funk);
		ElemsOfLife.addToGenre(Dance);
		SrchOfSunrise.addToGenre(Elec);
		WhosNext.addToGenre(Rock);
		Tommy.addToGenre(Rock);
		
		uberCat.addToCatalogue(Elec);
		uberCat.addToCatalogue(Pop);
		uberCat.addToCatalogue(Alt);
		uberCat.addToCatalogue(Jazz);
		uberCat.addToCatalogue(Rock);
	}
	
	@Test
	public void test1() {
		Set<Element> result = new HashSet<Element>();
		result.add(PageOne);
		result.add(OOTB);
		result.add(ElemsOfLife);
		assertEquals(result, new HashSet<Element>(uberCat.queryCat("matches (\"One\") || (in (\"Dance\") && by (\"Tiesto\"))")));
	}
	
	@Test
	public void test2() {
		Set<Element> result = new HashSet<Element>();
		result.add(Cross);
		result.add(RAM);
		result.add(Discovery);
		result.add(Homework);
		result.add(DmgCtl);
		result.add(ElemsOfLife);
		Set<Element> returnResult = new HashSet<Element>(uberCat.queryCat("in (\"Dance\") || (by (\"Justice\") && matches (\"Cross\"))"));
		assertEquals(result, returnResult);
	}
	
	@Test
	public void test3() {
		Set<Element> result = new HashSet<Element>();
		
		result.add(OOTB);
		result.add(TeenDream);
		result.add(Vol1);
		result.add(Vol2);
		result.add(Loud);
		result.add(AGLM);
		result.add(WhosNext);
		result.add(Presto);
		result.add(Rush);
		result.add(n2112);
		result.add(Signals);
		assertEquals(result, new HashSet<Element>(uberCat.queryCat("(in (\"Pop\") || (matches (\"Who\") || by (\"Rush\"))")));
	}

}
