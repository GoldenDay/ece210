package ca.ubc.ece.eece210.mp4.ast;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp4.Album;
import ca.ubc.ece.eece210.mp4.Catalogue;
import ca.ubc.ece.eece210.mp4.Element;
import ca.ubc.ece.eece210.mp4.Genre;

public class AndNodeTest {
	
	Catalogue testCatalogue = new Catalogue();
	
	Album Beethoven_Symphonies;
	Album Chopin_Nocturnes;
	Album Chopin_Symphonies;
	Album Jazz_Symphonies;
	Album Louis_and_the_Angels;
	Album Crossings;
	
	Genre Baroque;
	Genre Classical;
	Genre Romantic;
	Genre Jazz;
	Genre Psychedelic_Jazz;
	
	ArrayList<String> emptySongs = new ArrayList<String>();
	
	//instantiate some different andNodes, inNodes, matchesNode for checking
	ASTNode andTestRootNode; 
	ASTNode andTestSecondNode;
	ASTNode inTestNode;
	ASTNode inTestNode2;
	ASTNode matchTestNode;
			
	Set<Element> expectedSet;
	Set<Element> resultSet;

	
	@Before
	public void initialize() {
		
		
		Classical = new Genre("Classical");
		Baroque = new Genre("Baroque");
		Baroque.addToGenre(Classical);
		Romantic = new Genre("Romantic");
		Romantic.addToGenre(Classical);
		
		//should we be REQUIRED to add to Music? - NOTE: These genres not linked to music
		Jazz = new Genre("Jazz");
		Psychedelic_Jazz = new Genre("Psychedelic Jazz");
		Psychedelic_Jazz.addToGenre(Jazz);
		
		/*Ideally, we REQUIRE an addToCatalogue() method so the interpret() method can do its work nicely
		 *  - now implemented.  */
		testCatalogue.addToCatalogue(Jazz);
		testCatalogue.addToCatalogue(Classical);
		
		Beethoven_Symphonies = new Album("Symphonies","Beethoven", emptySongs);
		Beethoven_Symphonies.addToGenre(Classical);

		Chopin_Nocturnes = new Album("Nocturnes", "Chopin", emptySongs);
		Chopin_Nocturnes.addToGenre(Romantic);
		
		Chopin_Symphonies = new Album("Chopin Symphonies", "Chopin", emptySongs); //assume that matches works with regexes correctly
		Chopin_Symphonies.addToGenre(Romantic);
		
		Jazz_Symphonies = new Album("Jazz Symphonies", "Whoever", emptySongs);
		Jazz_Symphonies.addToGenre(Jazz);
		
		Crossings = new Album("Crossings", "Herbie Hancock", emptySongs);
		Crossings.addToGenre(Psychedelic_Jazz);
		
		Louis_and_the_Angels = new Album("Louis and the Angels", "Louis Armstrong", emptySongs);
		Louis_and_the_Angels.addToGenre(Jazz);
		
	}
	
	//Thought: What if I used the wrong token to construct this node? - OK, I won't test this, LOL
	
	@Test
	public void setArgumentTest() {
		try {
			andTestRootNode = new AndNode( Token.getTokenInstance("&&") );
			andTestRootNode.setArguments("something");
			assertNull("AndNode should not have argument - rep invar is broken.", andTestRootNode.arguments);
		} catch (InvalidTokenException e) {
			//won't happen
		}
		
	}
	
	
	@Test
	public void addChildTest() {
		try{
			andTestRootNode = new AndNode( Token.getTokenInstance("&&") );
			andTestSecondNode = new AndNode( Token.getTokenInstance("&&") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			andTestRootNode.addChild(inTestNode);
			andTestRootNode.addChild(matchTestNode);
			andTestRootNode.addChild(andTestSecondNode);
			assertEquals("AndNode should have 2 children - rep. invar broken", 2, andTestRootNode.children.size());
		} catch (InvalidTokenException e){
			//this won't happen here
		}
	}
	
	/**
	 * Note: The following tests instantiate inNode, and matchesNode objects, 
	 * and requires respective interpret() functioning properly for these tests to pass.
	 */
	@Test
	public void testInterpretSingleMatch() {
		try {
			expectedSet = new HashSet<Element>();
			expectedSet.add(Crossings);
			
			//in Jazz and matches Crossings, Note: these NOT linked to music
			andTestRootNode = new AndNode( Token.getTokenInstance("&&") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Psychedelic Jazz");
			matchTestNode.setArguments("Crossings");
			
			andTestRootNode.addChild(inTestNode);
			andTestRootNode.addChild(matchTestNode);
			resultSet = andTestRootNode.interpret(testCatalogue);
			
			//JUnit assertEquals(expected, actual) uses the Set.equals method
			assertEquals(expectedSet, resultSet);
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	@Test
	public void testInterpretOneEmptyNode() {
		try {
			//in Baroque and matches Nocturne - empty intersected with non-empty
			andTestRootNode = new AndNode( Token.getTokenInstance("&&") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Baroque");
			matchTestNode.setArguments("Nocturnes");
			
			andTestRootNode.addChild(inTestNode);
			andTestRootNode.addChild(matchTestNode);
			resultSet = andTestRootNode.interpret(testCatalogue);
			
			assertTrue(resultSet.isEmpty());
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	@Test
	public void testInterpretTwoEmptyNodes() {
		try {
			//in Baroque and matches Something -empty intersected with empty
			andTestRootNode = new AndNode( Token.getTokenInstance("&&") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Baroque");
			matchTestNode.setArguments("Something");
				
			andTestRootNode.addChild(inTestNode);
			andTestRootNode.addChild(matchTestNode);
			resultSet = andTestRootNode.interpret(testCatalogue);
				
			assertTrue(resultSet.isEmpty());
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	@Test
	public void testInterpretDoubleMatch() {
		try {
			expectedSet = new HashSet<Element>();
			expectedSet.add(Chopin_Nocturnes);
			
			//in Classical and matches Symphonies
			andTestRootNode = new AndNode( Token.getTokenInstance("&&") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Romantic"); //changed
			matchTestNode.setArguments("Nocturnes");
			
			andTestRootNode.addChild(inTestNode);
			andTestRootNode.addChild(matchTestNode);
			resultSet = andTestRootNode.interpret(testCatalogue);
			
			//JUnit assertEquals(expected, actual) uses the Set.equals method
			assertEquals(expectedSet, resultSet);
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
	
	@Test
	public void testInterpretNestedAndCall() {
		try {
			//expectedSet = new HashSet<Element>();
			//expectedSet.add(Chopin_Symphonies);
			
			//in Classical AND in Romantic AND matches Nocturnes
			andTestRootNode = new AndNode( Token.getTokenInstance("&&") );
			andTestSecondNode = new AndNode( Token.getTokenInstance("&&") );
			inTestNode = new InNode( Token.getTokenInstance("in"));
			inTestNode2 = new InNode( Token.getTokenInstance("in"));
			matchTestNode = new MatchesNode( Token.getTokenInstance("matches"));
			
			inTestNode.setArguments("Classical");
			matchTestNode.setArguments("Symphonies");
			
			andTestSecondNode.addChild(inTestNode);
			andTestSecondNode.addChild(matchTestNode);
			
			inTestNode2.setArguments("Romantic");
			
			andTestRootNode.addChild(inTestNode2);
			andTestRootNode.addChild(andTestSecondNode);
			
			resultSet = andTestRootNode.interpret(testCatalogue);
		
			//JUnit assertEquals(expected, actual) uses the Set.equals method
			assertTrue(resultSet.isEmpty());
		} catch (InvalidTokenException e) {
			//this won't happen
		}
	}
}
