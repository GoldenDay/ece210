package ca.ubc.ece.eece210.mp4;

import java.io.*;
import java.util.*;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp4.Album;
import ca.ubc.ece.eece210.mp4.Catalogue;
import ca.ubc.ece.eece210.mp4.Genre;

public class CatalogueTest {
	
	String fileName;
	PrintStream output;
	
	Catalogue catalogueToSave = new Catalogue();
	Catalogue catalogueRestored = new Catalogue();
	
	Album Beethoven_Symphonies;
	Album Haydn_Sonatas;
	Album Chopin_Nocturnes;
	Genre Baroque;
	Genre Classical;
	Genre Romantic;
	Genre Music;
	Genre Other;
	
	ArrayList<String> beethovenSongs = new ArrayList<String>();
	ArrayList<String> haydnSongs = new ArrayList<String>();
	ArrayList<String> chopinSongs = new ArrayList<String>();
	
	@Before
	public void initialize() throws FileNotFoundException{
		fileName = "saveLocation.txt";
		output = new PrintStream(fileName);
		
		beethovenSongs.add("Symphony No. 5");
		beethovenSongs.add("Eroica");
		haydnSongs.add("Sonata 7");
		chopinSongs.add("Nocturne in C Sharp minor");
		chopinSongs.add("Nocturne in A minor");
		chopinSongs.add("Nocturne 17");
		
		Music = new Genre("Music");
		
		Classical = new Genre("Classical");
		Classical.addToGenre(Music);
		Baroque = new Genre("Baroque");
		Baroque.addToGenre(Classical);
		Romantic = new Genre("Romantic");
		Romantic.addToGenre(Classical);
		Other = new Genre("Other");
		Other.addToGenre(Music);
		
		Beethoven_Symphonies = new Album("Symphonies","Beethoven", beethovenSongs);
		Beethoven_Symphonies.addToGenre(Classical);
		
		Haydn_Sonatas = new Album("Sonatas","Haydn", haydnSongs);
		Haydn_Sonatas.addToGenre(Classical);
		
		Chopin_Nocturnes = new Album("Nocturnes", "Chopin", chopinSongs);
		Chopin_Nocturnes.addToGenre(Romantic);
		
		
	}
	
	@Test
	public void saveCatalogueToFileTest() { 
		
		catalogueToSave.saveCatalogueFromRoot(Music, output);

	}
	
	@Test
	public void recreateLibraryFromFileTest() throws FileNotFoundException {
		
		/* Assuming previous test passed correctly, this should restore the collection from the
		 * file string Representation - we manually check that things are the same.
		 */
		
		catalogueToSave.saveCatalogueFromRoot(Music, output);
		try {
			catalogueRestored.recreateLibraryFromFile(fileName);
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
		
		catalogueRestored.saveCatalogueToFile("file2.txt");
	}

}
