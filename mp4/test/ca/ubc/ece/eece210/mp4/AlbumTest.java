package ca.ubc.ece.eece210.mp4;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp4.Album;
import ca.ubc.ece.eece210.mp4.Genre;

public class AlbumTest {
	ArrayList<String> songsToAdd = new ArrayList<String>();
	Genre metal;
	
	@Before
	public void initialize(){
		metal = new Genre("Metal");
		songsToAdd.add("Super Mission");
		songsToAdd.add("Retrospection");
	}

	@Test
	public void addToGenreTest() {
		Album jetsnGunsGold = new Album("Jets n Guns Gold", "Machinae Supremacy", songsToAdd);
		jetsnGunsGold.addToGenre(metal);
		assertEquals(metal, jetsnGunsGold.getGenre());
	}
	
	@Test
	public void removeFromGenreTest(){
		Album jetsnGunsGold = new Album("Jets n Guns Gold", "Machinae Supremacy", songsToAdd);
		jetsnGunsGold.addToGenre(metal);
		jetsnGunsGold.removeFromGenre();
		assertTrue(metal != jetsnGunsGold.getGenre());
		assertFalse(metal.getChildren().contains(jetsnGunsGold));
	}
	
	@Test
	public void getStringRepresentationTest(){
		Album jetsnGunsGold = new Album("Jets n Guns Gold", "Machinae Supremacy", songsToAdd);
		jetsnGunsGold.addToGenre(metal);
		System.out.println(jetsnGunsGold.getStringRepresentation());
		assertTrue(jetsnGunsGold.getStringRepresentation().equals("<Album>Metal\tJets n Guns Gold\tMachinae Supremacy\tSuper Mission\tRetrospection\t<\\Album>"));
	}
	
	@Test
	public void fromStringRepresentationTest() {
		Album jetsnGunsGold = new Album("<Album>Metal\tJets n Guns Gold\tMachinae Supremacy\tSuper Mission\tRetrospection\t<\\Album>");
		assertTrue(jetsnGunsGold.getPerformer().equals("Machinae Supremacy"));
		assertTrue(jetsnGunsGold.getName().equals("Jets n Guns Gold"));
		ArrayList<String> songList = jetsnGunsGold.getSongList();
		assertTrue(songList.equals(songsToAdd));
	}
	
	@Test
	public void getGenreTest(){
		Album jetsnGunsGold = new Album("Jets n Guns Gold", "Machinae Supremacy", songsToAdd);
		jetsnGunsGold.addToGenre(metal);
		assertEquals(metal, jetsnGunsGold.getGenre());
	}
	
	@Test
	public void getTitleTest(){
		Album jetsnGunsGold = new Album("Jets n Guns Gold", "Machinae Supremacy", songsToAdd);
		assertTrue(jetsnGunsGold.getName().equals("Jets n Guns Gold"));
	}
	
	@Test
	public void getPerformerTest(){
		Album jetsnGunsGold = new Album("Jets n Guns Gold", "Machinae Supremacy", songsToAdd);
		assertTrue(jetsnGunsGold.getPerformer().equals("Machinae Supremacy"));
	}
	
	@Test
	public void equalsHashTest(){
		Album test1 = new Album("X & Y", "Coldplay", null);
		Album test2 = new Album("X & Y", "Coldplay", null);
		Album test3 = new Album("Human After All", "Daft Punk", null);
		assertEquals(test1, test2);
		assertFalse(test3.equals(test2));
	}

}
