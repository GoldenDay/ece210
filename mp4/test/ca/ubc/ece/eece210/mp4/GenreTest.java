package ca.ubc.ece.eece210.mp4;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp4.Album;
import ca.ubc.ece.eece210.mp4.Genre;

public class GenreTest {
        ArrayList<String> songsToAdd = new ArrayList<String>();
        Album jetsnGunsGold;
        Genre rock;
        Genre punkRock;
        Genre metal;
        Genre testRock;
        Genre rockWithoutParent;

        @Before
        public void initialize(){
                songsToAdd.add("Super Mission");
                songsToAdd.add("Retrospection");
                
                rock = new Genre("Rock");                       
                punkRock = new Genre("Punk Rock");
                metal = new Genre("Metal");
                jetsnGunsGold = new Album("Jets n Guns Gold", "Machinae Supremacy", songsToAdd);
        }
        
        @Test
        public void getParentAddToGenreTest() {
                punkRock.addToGenre(rock);
                metal.addToGenre(rock);
                
                assertEquals(rock, punkRock.getParent());
                assertEquals(rock, metal.getParent());
        }
        
        @Test
        public void getStringRepresentationTest(){
        	punkRock.addToGenre(rock);
            	metal.addToGenre(rock);
            	
        	System.out.println(rock.getStringRepresentation());
        	System.out.println(punkRock.getStringRepresentation());
        	assertTrue(rock.getStringRepresentation().equals("<Genre>\tRock\t<\\Genre>"));
        	assertTrue(punkRock.getStringRepresentation().equals("<Genre>\tRock\tPunk Rock<\\Genre>"));
        }
        
        @Test
        public void restoreGenreTest(){
        	testRock = new Genre("whatever");
        	testRock = Genre.restoreGenre("<Genre>\tRock\tPunk Rock<\\Genre>");
        	assertTrue(testRock.getName().equals("Punk Rock"));
        	System.out.println(testRock.getName());
        	
        	rockWithoutParent = new Genre("another");
        	rockWithoutParent = Genre.restoreGenre("<Genre>\tRock without Parent\t<\\Genre>");
        	assertTrue(rockWithoutParent.getName().equals("Rock without Parent"));
        	System.out.println(rockWithoutParent.getName());
        }
        
        @Test
        public void equalsHashTest() {
        	Genre rock = new Genre("Rock");
        	Genre metal1 = new Genre("Metal");
        	Genre metal2 = new Genre("Metal");
        	assertTrue(metal1.equals(metal2));
        	
        	metal1.addToGenre(rock);
        	metal2.addToGenre(rock);
        	assertTrue(metal1.equals(metal2));
        	
        	assertEquals(metal1.hashCode(), metal2.hashCode());
        }

}
