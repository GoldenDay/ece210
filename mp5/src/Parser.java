/*
 * For the Marvel Edge file
 * File format: each line is one character and its book seperated by a tab
 */
import java.io.*;
import java.util.*;

public class Parser {
  
	  /*
	   * @param filename
	   * @param characters list in which all character names will be stored;
	   * @param books map from titles of comic books to characters that appear in them; 
	   * @modifies characters, books
	   * @effects fills "characters" with a list of all unique character names
	   * @effects fills "books" with a map from each comic book to all characters in it
	   */
	  public static void parseFile(String filename, Set<String> characters,
	          Map<String, List<String>> books) throws Exception {

	    BufferedReader reader = null;
	    
	    try {
	    	reader = new BufferedReader(new FileReader(filename));

	    	// Construct the collections of characters and books, <character, book>, a pair at a time.
	    	String inputLine;
	    	while ((inputLine = reader.readLine()) != null) {
	    	  
	    		if (inputLine.startsWith("#")) {
	    			continue;
	        }

	        // Gets rid of quotations, parses with exception if line with wrong structure
	        inputLine = inputLine.replace("\"", "");
	        String[] tokens = inputLine.split("\t");
	        if (tokens.length != 2) {
	          throw new Exception("Line should contain exactly one tab: " + inputLine);
	        }

	        String character = tokens[0];
	        String book = tokens[1];

	        // Add the parsed data to the character and book collections.
	        characters.add(character);
	        if (!books.containsKey(book)) {
	          books.put(book, new ArrayList<String>());
	        }
	        books.get(book).add(character);
	        
	      }
	    	
	    } catch (IOException e) {
	      System.err.println(e.toString());
	      e.printStackTrace(System.err);
	    } finally {
	      if (reader != null) {
	        reader.close();
	      }
	    }
	  }
	  
}
