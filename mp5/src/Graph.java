
/*
 * Graph with node connected by its labeled edges
 * Labels - Book names
 * Nodes - Characters
 * children != null 
 */
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class Graph<E extends Comparable<E>, T extends Comparable<T>> {
	private Map<E, Set<Edge<E,T>>> children;
	
    /*
     *Constructs a new Graph, with no nodes and no edges.
     */
	public Graph() {
		this.children = new HashMap<E, Set<Edge<E,T>>>();
	}
	
	
    /*
     * Constructs a new Graph, with nodes as the given set of nodes.
     * @param set of nodes
     * @require passed in set of nodes != null	
     */	
	public Graph(Set<E> nodes) {
		Map<E, Set<Edge<E,T>>> constr = new HashMap<E, Set<Edge<E,T>>>();
		for(E node : nodes) {
			constr.put(node, new HashSet<Edge<E,T>>());
		}
		this.children = constr;
	}
	
	
	/*
	 * Builds the graph according to a given map
	 * @param map
	 */
	public Graph(Map<E, Set<Edge<E,T>>> map) {
		this.children = map;
	}
	
	
	/*
	 * Adds an Edge to the graph, under the conditions that (no duplicates)
	 * @param parent, child, label
	 * @requires parent != null && child != null && label != null
	 * if 		parent and child node is both contained in nodes and edges 
	 * then		does not already include edge with same parent child and label. 
	 */
	public boolean addEdge(E parent, E child, T label) {
			Edge<E,T> edge = new Edge<E,T>(parent, child, label);
			if(children.containsKey(parent)) {
				children.get(parent).add(edge);				
			} else {
				Set<Edge<E,T>> temp = new HashSet<Edge<E,T>>();
				temp.add(edge);
				children.put(parent, temp);
			}
			return true;
	}
	
	
	/*
	 * Returns a sorted list of all the child nodes of a parent node
	 * @param node, the parent node for which we are looking for children
	 * @requires node != null
	 * @return 	a sorted list of child nodes for the parent node, 
	 * 			including the labels of its edge alphabetically
	 */
	public Set<Edge<E,T>> listChildren(E node) {
		if(children.containsKey(node)) {
			return children.get(node);
		} else {
			return new HashSet<Edge<E,T>>();
		}
	}
}
