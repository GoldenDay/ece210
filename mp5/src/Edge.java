/*
 * The Edge of Graph
 * Includes 2 nodes and its label (relationship)
 * And edge MUST contain the 2 two nodes and its label
 */

public class Edge < E extends Comparable <E>, T extends Comparable <T> > implements Comparable < Edge <E, T> >{
  
	private E parent;
	private E child;
	private T label;

	/*
	 *  requires parent, child, label != null
	 */
	public Edge(E parent, E child, T label) {
		this.parent = parent;
		this.child = child;
		this.label = label;
		notNull();
	}
	
	/*
	 * Returns parent value (copy)
	 */
	public E parentNode() {
		E parentNode = parent;
		return parentNode;
	}
	
	/*
	 * Returns child value(copy)
	 */
	public E childNode() {
		E childNode = child;
		return childNode;
	}
	
	/*
	 * Returns label value(copy)
	 */
	public T getLabel() {
		T labelreturn = label;
		return labelreturn;
	}
	
	/*
	 * Make sure parent, child, label != null
	 */
	private void notNull(){
		if(this.parent == null || this.child == null || this.label == null) {
			throw new RuntimeException("parent, child, label != null requriment not made");
		}
	}
	
	/*
	 * Check for doubles
	 * @param other (Edge)
	 * other != null
	 * returns true if same edge
	 */
	public boolean isSameEdge( Edge<E, T> other ){
		return this.parent.equals(other.parentNode()) && this.child.equals(other.childNode());
	}
	
	/*
	 * checks if two edges have one's parent as child and on'es child as parent
	 */
	public boolean isFatherSon( Edge<E,T> other ){
		if(this.parent.equals(other.parentNode()) && this.child.equals(other.parentNode())) {
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Check if parent == child
	 */
	public boolean isMeAllAlong(){
		return this.parent.equals(this.child);
	}
	
	/*
	 * represent edge into string
	 * @returns string
	 */
	public String toString(){
		return this.parent + "[" + this.label + "]" + this.child;
	}

	@Override
	public int compareTo(Edge<E, T> arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
