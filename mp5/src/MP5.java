import java.io.Console;
import java.util.List;

/*
 * Find shortest path between two characters using command line
 * @requires set path to jdk1.7.0_40\bin
 * @requires labeled_edges.tsv to be in same folder as MP5.java
 * @requires file != null
 */
public final class MP5 {
	public static void main(String... Args){
		Console console = System.console();
		console.printf(	"This program looks through the marvel universe. \n"
						+ "We will find the closest path between \n two characters using books and characters. \n"
						+ "Please note that all characters and books \n must follow predefined abbreviations.\n");
		
		String character1 = console.readLine("Who is the character you would like to search with? \n");
		String character2 = console.readLine("Who is the second character you would \n like to search the relationship path with the first?\n");
		
		console.printf(	"We will now try to find the shortest \n relationship path betwen " + character1 + "and "+ character2 + ".\n"
				+ "...\n...\n...\n");
		
		Graph<String, String> yourmom = Path.buildGraph("labeled_edges.tsv");
		List<Edge<String, String>> RAWR = Path.getShortestPath(yourmom, character1, character2);
		if (RAWR != null){
			console.printf(	"\n Here is our result. \nThe shortest path between " + character1 +"and "+ character2 + " is:\n" +RAWR.toString() 
						+"\n HOW TO INTERPRET RESULT:\n Chacter 1 [Book that they share] Character 2. \n K THNX BYE!");
		} else {
			console.printf("\n NO PATH FOUND. SO SAD. QQ"); // No path found.
		}
	}
	
}