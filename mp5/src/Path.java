import java.util.*;

/*
 * Builds a graph from file using Parser
 * Finds shortest path between two nodes in graph
 */
public class Path {

	/*
	 *Takes in a file name and builds a graph
	 * @param filename
	 * @requires file != null
	 * @return built graph
	 */
	public static Graph<String, String> buildGraph(String filename) {
		Set<String> characters = new HashSet<String>();
		Map<String, List<String>> books = new HashMap<String, List<String>>();
		
		try {
			Parser.parseFile(filename, characters, books);
		} catch (Exception e) {
			e.printStackTrace();  
		}
		
		return finishGraph(characters, books);
	}

	private static Graph<String, String> finishGraph(Set<String> nodes, Map<String, List<String>> books) {
		Graph<String, String> graph = new Graph<String, String>(nodes);
		
		for(String book : books.keySet()) {
			List<String> characters = books.get(book);
			
			// every character in the book adds an edge to itself and any other character in the book.
			for(int i = 0; i < characters.size(); i++) {
				
				for(int j = i +1; j < characters.size(); j++) {
					graph.addEdge(characters.get(i), characters.get(j), book);
					graph.addEdge(characters.get(j), characters.get(i), book);
				}
			}
		}
		return graph;
	}
	
	
	/*
	 * Finds the shortest path
	 * @param graph, start node, end node
	 * @return a list of edges which connect
	 */
	public static List < Edge<String, String> > 
	getShortestPath(Graph <String, String> graph, String start, String end) {
		
		// stores the corresponding path for every node looked
		Map<String, List<Edge<String, String>>> paths = new HashMap<String, List<Edge<String, String>>>(); 
		
		// stores a collection of nodes to look at
		Queue<String> worklist = new LinkedList<String>();
		worklist.add(start);
		paths.put(start, new ArrayList<Edge<String, String>>());
		
		while(!worklist.isEmpty()) {
			
			// looks at the remaining nodes in "worklist"
			String node = worklist.remove();
			if(node.equals(end)) {
				
				// when node is end, returns the path corresponding to the end node
				return paths.get(node);
				
			} else {
				
				// explores all the children nodes for the given node
				for(Edge<String, String> e : graph.listChildren(node)) {
					String m = e.childNode();
					
					// if node is not already on the queue, adds node to queue and adds corresponding path into the paths. 
					if(!paths.containsKey(m) && paths.get(node) != null) {
						List<Edge<String, String>> list = paths.get(node);
						List<Edge<String, String>> newpath = new ArrayList<Edge<String, String>>(list);
						
						newpath.add(e);
						paths.put(m, newpath);
						worklist.add(m);
					}
				}
			}
		}
		return null;
	}
}
