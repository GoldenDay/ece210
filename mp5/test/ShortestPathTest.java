import static org.junit.Assert.*;
import java.util.List;
import org.junit.Test;

public class ShortestPathTest {

	/*
	 * One-EdgePath
	 * path from BEAST/HENRY &HANK& P to WISDOM, PETER
	 * expected BEAST/HENRY &HANK& P to WISDOM, PETER linked by [X:PRIME]
	 */
	@Test
	public void OneEdgePathTest1() {
		Graph<String, String> yourmom = Path.buildGraph("src/labeled_edges.tsv");
		List<Edge<String, String>> RAWR = Path.getShortestPath(yourmom, "BEAST/HENRY &HANK& P", "WISDOM, PETER");
		System.out.println(RAWR.toString());
		
	}
		
	@Test
	public void OneEdgePathTest2() {
		Graph<String, String> yourmom = Path.buildGraph("src/labeled_edges.tsv");
		List<Edge<String, String>> RAWR = Path.getShortestPath(yourmom, "GORILLA-MAN", "VENUS II");
		System.out.println(RAWR.toString());
	}
	
	/*
	 * Two-EdgePath: Checks if two nodes can be connected by two jumps
	 * Create new graph with only 3 nodes connected by 2 edges
	 * added node NODE_A, NODE_B, NODE_C
	 * added edge EDGE_1 from NODE_A to NODE_B
	 * added edge EDGE_2 from NODE_B to NODE_C
	 */
	@Test
	public void SimpleTwoEdgePathTest(){
		Graph<String, String> graph1 = new Graph<String, String>();

		graph1.addEdge("Node_A", "Node_B", "Edge_1");
		graph1.addEdge("Node_B", "Node_C", "Edge_2");
		
		List<Edge<String, String>> RAWR = Path.getShortestPath(graph1, "Node_A", "Node_C");
		System.out.println(RAWR.toString());
		
		//IMPORTANT: UNLIKE GRAPHS BY PARSER.JAVA, NO BIDIRECTIONAL EDGES, THIS ONE MUST BE NULL
		List<Edge<String, String>> RAWR2 = Path.getShortestPath(graph1, "Node_C", "Node_A");
		assertNull(RAWR2);
	}
	
	/*
	 * Two-EdgePath: Checks if two nodes can be connected by two jumps
	 * path from FOROPULIST to VENUS II
	 * expected path: FOROPULIST[AA2 26]HAWK, HAWK[AVF 5]VENUS II
	 */
	@Test
	public void TwoEdgePathTest() {
		Graph<String, String> yourmom = Path.buildGraph("src/labeled_edges.tsv");
		List<Edge<String, String>> RAWR = Path.getShortestPath(yourmom, "FOROPULIST", "VENUS II");
		System.out.println(RAWR.toString());
		
		//BIDIRECTIONAL IN THIS CASE
		List<Edge<String, String>> RAWR2 = Path.getShortestPath(yourmom, "VENUS II", "FOROPULIST");
		System.out.println(RAWR2.toString());
		
	}
}
